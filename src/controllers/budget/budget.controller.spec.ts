import { Test, TestingModule } from '@nestjs/testing';
import { getModelToken } from '@nestjs/mongoose';
import { MongoService } from '@gateway/mongo.service';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { KeyRequest } from '@infrastructure/enum/KeyRequest';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Message } from '@infrastructure/enum/Message';
import * as request from 'supertest';
import { BudgetController } from '@controllers/budget/budget.controller';
import { BudgetService } from '@controllers/budget/budget.service';
import { Budget } from '@schemas/budget.schema';
import { Observable, of } from 'rxjs';
import { BudgetCategoryDTO, BudgetDTO } from '@dto/BudgetDTO';
import { BudgetCategory } from '@schemas/category.schema';
import { PaymentMethod } from '@infrastructure/enum/PaymentMethod';

describe('UserController', () => {
  let mongoService: MongoService;
  let budgetController: BudgetController;
  let app: INestApplication;
  const budgetCategory: BudgetCategoryDTO = {
    name: 'testing',
  };
  const budget: BudgetDTO = {
    amount: 100,
    description: 'testing',
    categoryId: '646c39f0da5dd6bc18b5809b',
    paymentMethod: PaymentMethod.debit,
  };

  const saveSpy = jest.spyOn(MongoService.prototype, 'save');

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [BudgetController],
      providers: [
        BudgetService,
        MongoService,
        {
          provide: getModelToken(BudgetCategory.name),
          useValue: jest.fn(),
        },
        {
          provide: getModelToken(Budget.name),
          useValue: jest.fn(),
        },
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();

    budgetController = moduleRef.get<BudgetController>(BudgetController);
    mongoService = await moduleRef.resolve(MongoService);
  });

  afterEach(() => {
    saveSpy.mockClear();
  });

  describe('Budget controller testing', () => {
    it('should return a message that budget category was created with status code 200', async () => {
      saveSpy.mockImplementation(
        (): Observable<ResponseMapping> =>
          of({
            statusCode: HttpStatus.OK,
            key: KeyRequest.MESSAGE,
            body: Message.BUDGET_CATEGORY_CREATED,
          }),
      );

      await request(app.getHttpServer())
        .post('/budgets/category')
        .send({ ...budgetCategory })
        .expect(({ body }) => {
          expect(body).toHaveProperty(
            KeyRequest.MESSAGE,
            Message.BUDGET_CATEGORY_CREATED,
          );
        })
        .expect(HttpStatus.OK);

      expect(saveSpy).toBeCalledTimes(1);
    });

    it('should return a message that budget was created with status code 200', async () => {
      saveSpy.mockImplementation(
        (): Observable<ResponseMapping> =>
          of({
            statusCode: HttpStatus.OK,
            key: KeyRequest.MESSAGE,
            body: Message.BUDGET_CREATED,
          }),
      );

      await request(app.getHttpServer())
        .post('/budgets')
        .send(budget)
        .set('Authorization', `${process.env.PD_REQUEST_TOKEN}`)
        .expect(({ body }) => {
          expect(body).toHaveProperty(
            KeyRequest.MESSAGE,
            Message.BUDGET_CREATED,
          );
        })
        .expect(HttpStatus.OK);

      expect(saveSpy).toBeCalledTimes(1);
    });
  });
});
