import { Body, Controller, Get, Post, Req, Res } from '@nestjs/common';
import { BudgetRepository } from '@controllers/budget/budget.repository';
import { BudgetCategoryDTO, BudgetDTO } from '@dto/BudgetDTO';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Request, Response } from 'express';
import { BudgetService } from '@controllers/budget/budget.service';
import { FilterQuery } from 'mongoose';

@Controller('budgets')
export class BudgetController implements BudgetRepository {
  constructor(private budgetService: BudgetService) {}
  @Post()
  public async createBudget(
    @Body() budgetDTO: BudgetDTO,
    @Res() res: Response,
    @Req() req: Request,
  ): Promise<Response> {
    const response: ResponseMapping = await this.budgetService.createBudget({
      ...budgetDTO,
      userId: req.sub.toString(),
    });

    return res
      .status(response.statusCode)
      .json({ [response.key]: response.body });
  }

  @Post('category')
  public async createCategory(
    @Body() budgetCategoryDTO: BudgetCategoryDTO,
    @Res() res: Response,
  ): Promise<Response> {
    const response: ResponseMapping = await this.budgetService.createCategory(
      budgetCategoryDTO,
    );

    return res
      .status(response.statusCode)
      .json({ [response.key]: response.body });
  }
  @Get()
  public async getBudgets(
    filter: FilterQuery<BudgetDTO>,
    @Res() res: Response,
    @Req() req: Request,
  ): Promise<Response> {
    filter = {
      userId: req.sub.toString(),
    };

    const response: ResponseMapping = await this.budgetService.getBudgets(
      filter,
    );

    return res
      .status(response.statusCode)
      .json({ [response.key]: response.body });
  }
}
