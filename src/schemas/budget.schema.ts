import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as SchemaM } from 'mongoose';
import * as moment from 'moment';
import { User } from '@schemas/user.schema';
import { BudgetCategory } from '@schemas/category.schema';
import { PaymentMethod } from '@infrastructure/enum/PaymentMethod';

export type BudgetDocument = HydratedDocument<Budget>;

@Schema()
export class Budget {
  @Prop({ required: true })
  amount: number;

  @Prop({ required: true })
  description: string;

  @Prop({
    ref: BudgetCategory.name,
    type: SchemaM.Types.ObjectId,
    required: true,
  })
  categoryId: string;

  @Prop({ ref: User.name, type: SchemaM.Types.ObjectId })
  userId?: string;

  @Prop({ required: true, enum: PaymentMethod, default: PaymentMethod.debit })
  paymentMethod: string;

  @Prop({ default: moment().unix() })
  created?: number;

  @Prop({ default: null })
  updated?: number;
}

export const BudgetSchema: SchemaM<Budget> =
  SchemaFactory.createForClass(Budget);
