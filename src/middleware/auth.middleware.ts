import { HttpStatus, Injectable, NestMiddleware, Scope } from '@nestjs/common';
import { NextFunction, Request, Response } from 'express';
import { JwtPayload, verify, VerifyErrors } from 'jsonwebtoken';
import { get } from 'lodash';
import { Message } from '@infrastructure/enum/Message';

@Injectable({ scope: Scope.REQUEST })
export class AuthMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    if (req.headers.authorization === undefined) {
      return res
        .status(HttpStatus.UNAUTHORIZED)
        .json({ message: Message.UNAUTHORIZED });
    }

    const token: string | string[] = req.headers.authorization;

    verify(
      `${token}`,
      `${process.env.PD_SECRET}`,
      (err: VerifyErrors, payload: string | JwtPayload) => {
        if (err)
          return res
            .status(HttpStatus.FORBIDDEN)
            .json({ message: Message.FORBIDDEN });

        req.sub = get(payload, '_id', '');

        next();
      },
    );
  }
}
