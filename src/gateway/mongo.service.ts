import { MongoRepository } from '@gateway/mongo.repository';
import { HttpStatus, Injectable } from '@nestjs/common';
import { Error, FilterQuery, HydratedDocument, Model } from 'mongoose';
import { catchError, mergeMap, Observable, of } from 'rxjs';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { KeyRequest } from '@infrastructure/enum/KeyRequest';
import { ErrorMapping } from '@infrastructure/mapping/ErrorMapping';

@Injectable()
export class MongoService implements MongoRepository {
  public findOne<K = object, T = object>(
    model: Model<K>,
    filterQuery: FilterQuery<T>,
  ): Observable<T> {
    return of(1).pipe(
      mergeMap(async (): Promise<T> => model.findOne(filterQuery) as T),
    );
  }
  public findAll<K = object, T = object>(
    model: Model<K>,
    filterQuery?: FilterQuery<T>,
    key?: string,
  ): Observable<ResponseMapping> {
    return of(1).pipe(
      mergeMap(
        async (): Promise<T[]> => model.find(filterQuery) as unknown as T[],
      ),
      mergeMap((data: unknown[]) => of(data)),
      catchError((err) => of(new Error.CastError('', err, ''))),
      mergeMap((err: Error.CastError) =>
        of(this._validateResponse(err, undefined, key)),
      ),
    );
  }
  public save<K = object, T = object>(
    model: Model<K>,
    dto: T,
    message?: string,
  ): Observable<ResponseMapping> {
    const document: HydratedDocument<K> = new model({ ...dto });

    return of(1).pipe(
      mergeMap(async () => await document.save()),
      catchError((err) => of(new Error.ValidatorError(err))),
      mergeMap((err: Error.ValidatorError) =>
        of(this._validateResponse(err, message)),
      ),
    );
  }
  private _validateResponse(
    err: Error,
    body?: string | object,
    key?: string,
  ): ResponseMapping {
    if (err instanceof Error.CastError) {
      const { value }: ErrorMapping = JSON.parse(JSON.stringify(err));

      return {
        statusCode: HttpStatus.BAD_REQUEST,
        body: value,
        key: KeyRequest.MESSAGE,
      };
    }

    if (err instanceof Error.ValidatorError) {
      const {
        properties: { errors },
      }: ErrorMapping = JSON.parse(JSON.stringify(err));

      return {
        statusCode: HttpStatus.BAD_REQUEST,
        body: errors,
        key: KeyRequest.MESSAGE,
      };
    }

    return {
      statusCode: HttpStatus.OK,
      body: !body ? err : body,
      key: !key ? KeyRequest.MESSAGE : key,
    };
  }
}
