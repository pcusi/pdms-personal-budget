export enum PaymentMethod {
  credit = 'credit',
  debit = 'debit',
  transfer = 'transfer',
}
