import { Budget } from '@schemas/budget.schema';
import { BudgetCategory } from '@schemas/category.schema';

export class BudgetDTO extends Budget {
  _id?: string;
}

export class BudgetCategoryDTO extends BudgetCategory {
  _id?: string;
}
