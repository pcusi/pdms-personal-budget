import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserController } from '@controllers/user/user.controller';
import { UserService } from '@controllers/user/user.service';
import { User, UserSchema } from '@schemas/user.schema';
import { MongoService } from '@gateway/mongo.service';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: User.name, schema: UserSchema }]),
  ],
  controllers: [UserController],
  providers: [UserService, MongoService],
})
export class UserModule {}
