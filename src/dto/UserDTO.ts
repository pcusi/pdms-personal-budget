import { User } from '@schemas/user.schema';

export class UserDTO extends User {
  id?: string;
}
