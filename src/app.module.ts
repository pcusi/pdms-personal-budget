import { MiddlewareConsumer, Module, RequestMethod } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import { UserModule } from '@controllers/user/user.module';
import { AuthMiddleware } from '@middleware/auth.middleware';
import { UserController } from '@controllers/user/user.controller';
import { BudgetModule } from '@controllers/budget/budget.module';
import { BudgetController } from '@controllers/budget/budget.controller';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
    }),
    MongooseModule.forRoot(
      `${process.env.PD_MONGO_URI}/${process.env.PD_STAGE}-${process.env.PD_MONGO_DATABASE}`,
    ),
    UserModule,
    BudgetModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer
      .apply(AuthMiddleware)
      .exclude({
        path: 'users/auth',
        method: RequestMethod.POST,
      })
      .forRoutes(UserController, BudgetController);
  }
}
