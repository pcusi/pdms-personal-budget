import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as SchemaM } from 'mongoose';
import * as moment from 'moment';

export type BudgetCategoryDocument = HydratedDocument<BudgetCategory>;

@Schema()
export class BudgetCategory {
  @Prop({ required: true })
  name: string;

  @Prop({ default: moment().unix() })
  created?: number;

  @Prop({ default: null })
  updated?: number;
}

export const BudgetCategorySchema: SchemaM<BudgetCategory> =
  SchemaFactory.createForClass(BudgetCategory);
