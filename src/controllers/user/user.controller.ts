import { Body, Controller, Post, Res } from '@nestjs/common';
import { UserService } from '@controllers/user/user.service';
import { UserDTO } from '@dto/UserDTO';
import { UserRepository } from '@controllers/user/user.repository';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Response } from 'express';
@Controller('users')
export class UserController implements UserRepository {
  constructor(private readonly userService: UserService) {}
  @Post()
  public async createUser(@Body() body: UserDTO): Promise<string> {
    return this.userService.createUser(body);
  }
  @Post('/auth')
  public async auth(
    @Body() userDTO: UserDTO,
    @Res() res: Response,
  ): Promise<Response> {
    const response: ResponseMapping = await this.userService.auth(userDTO);

    return res
      .status(response.statusCode)
      .json({ [response.key]: response.body });
  }
}
