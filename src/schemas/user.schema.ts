import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { HydratedDocument, Schema as SchemaM } from 'mongoose';
import * as moment from 'moment';

export type UserTypeDocument = HydratedDocument<User>;

@Schema()
export class User {
  @Prop({ required: true })
  names: string;

  @Prop({ required: true })
  lastnames: string;

  @Prop()
  email?: string;

  @Prop({ required: true })
  username?: string;

  @Prop({ required: true })
  password?: string;

  @Prop({ default: moment().unix() })
  created?: number;

  @Prop({ default: null })
  updated?: number;
}

export const UserSchema: SchemaM<User> = SchemaFactory.createForClass(User);
