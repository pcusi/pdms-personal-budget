import { UserDTO } from '@dto/UserDTO';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Response } from 'express';

export interface UserRepository {
  createUser(userDTO: UserDTO): Promise<string>;
  auth(userDTO: UserDTO, res?: Response): Promise<ResponseMapping | Response>;
}
