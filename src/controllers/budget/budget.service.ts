import { Injectable } from '@nestjs/common';
import { BudgetRepository } from '@controllers/budget/budget.repository';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { BudgetCategoryDTO, BudgetDTO } from '@dto/BudgetDTO';
import { mergeMap, Observable, of } from 'rxjs';
import { MongoService } from '@gateway/mongo.service';
import { InjectModel } from '@nestjs/mongoose';
import { FilterQuery, Model } from 'mongoose';
import { BudgetCategory } from '@schemas/category.schema';
import { Budget } from '@schemas/budget.schema';
import { Message } from '@infrastructure/enum/Message';
import { KeyRequest } from '@infrastructure/enum/KeyRequest';

@Injectable()
export class BudgetService implements BudgetRepository {
  constructor(
    @InjectModel(BudgetCategory.name)
    private budgetCategoryModel: Model<BudgetCategory>,
    @InjectModel(Budget.name)
    private budgetModel: Model<Budget>,
    private mongoService: MongoService,
  ) {}
  public createBudget(budget: BudgetDTO): Promise<ResponseMapping> {
    return of(1)
      .pipe(mergeMap(() => this._buildBudgetRequest(budget)))
      .toPromise();
  }
  public getBudgets(
    filterQuery: FilterQuery<BudgetDTO>,
  ): Promise<ResponseMapping> {
    return of(1)
      .pipe(
        mergeMap(() =>
          this.mongoService.findAll<Budget, BudgetDTO>(
            this.budgetModel,
            filterQuery,
            KeyRequest.BUDGETS,
          ),
        ),
      )
      .toPromise();
  }
  public createCategory(
    budgetCategoryDTO: BudgetCategoryDTO,
  ): Promise<ResponseMapping> {
    return of(1)
      .pipe(mergeMap(() => this._buildBudgetCategoryRequest(budgetCategoryDTO)))
      .toPromise();
  }
  private _buildBudgetCategoryRequest(
    budgetCategoryDTO: BudgetCategoryDTO,
  ): Observable<ResponseMapping> {
    return of(1).pipe(
      mergeMap(() =>
        this.mongoService.save<BudgetCategory, BudgetCategoryDTO>(
          this.budgetCategoryModel,
          budgetCategoryDTO,
          Message.BUDGET_CATEGORY_CREATED,
        ),
      ),
    );
  }
  private _buildBudgetRequest(
    budgetDTO: BudgetDTO,
  ): Observable<ResponseMapping> {
    return of(1).pipe(
      mergeMap(() =>
        this.mongoService.save<Budget, BudgetDTO>(
          this.budgetModel,
          budgetDTO,
          Message.BUDGET_CREATED,
        ),
      ),
    );
  }
}
