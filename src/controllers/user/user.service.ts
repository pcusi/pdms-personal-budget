import { HttpStatus, Injectable } from '@nestjs/common';
import { User, UserTypeDocument } from '@schemas/user.schema';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDTO } from '@dto/UserDTO';
import { compare, hash } from 'bcrypt';
import { UserRepository } from '@controllers/user/user.repository';
import { mergeMap, Observable, of } from 'rxjs';
import { MongoService } from '@gateway/mongo.service';
import { sign } from 'jsonwebtoken';
import { isEmpty } from 'lodash';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { KeyRequest } from '@infrastructure/enum/KeyRequest';
import { Message } from '@infrastructure/enum/Message';

@Injectable()
export class UserService implements UserRepository {
  constructor(
    @InjectModel(User.name) private userModel: Model<User>,
    private mongoService: MongoService,
  ) {}

  public auth(userDTO: UserDTO): Promise<ResponseMapping> {
    return of(1)
      .pipe(mergeMap(() => this._generateToken(userDTO)))
      .toPromise();
  }

  public async createUser(userDTO: UserDTO): Promise<string> {
    try {
      const user: UserTypeDocument = new this.userModel({
        ...userDTO,
        username: this._generateUsername(userDTO.names, userDTO.lastnames),
        password: await hash(userDTO.password, 9),
      });

      await user.save();

      return 'Created';
    } catch (e) {
      return e;
    }
  }

  private _generateToken(body: UserDTO): Observable<ResponseMapping> {
    return of(1).pipe(
      mergeMap(() => this._getPayload(body.username, body.password)),
      mergeMap((payload: object | undefined) =>
        of(this._authValidation(payload)),
      ),
    );
  }

  private _getPayload(
    username: string,
    password: string,
  ): Observable<object | undefined> {
    return of(1).pipe(
      mergeMap(() =>
        this.mongoService.findOne<User, UserDTO>(this.userModel, { username }),
      ),
      mergeMap(async (user: UserDTO): Promise<object> => {
        if (user !== null) {
          const is_auth: boolean = await compare(password, user.password);

          if (is_auth) return { ...user['_doc'] };

          return {};
        }

        return undefined;
      }),
    );
  }

  private _authValidation(payload: object | undefined): ResponseMapping {
    if (!payload)
      return {
        statusCode: HttpStatus.NOT_FOUND,
        body: Message.USER_NOT_FOUND,
        key: KeyRequest.MESSAGE,
      };

    if (isEmpty(payload))
      return {
        statusCode: HttpStatus.BAD_REQUEST,
        body: Message.PASSWORD_NOT_MATCH,
        key: KeyRequest.MESSAGE,
      };

    return {
      statusCode: HttpStatus.OK,
      body: sign(payload, `${process.env.PD_SECRET}`, {
        expiresIn: process.env.PD_EXPIRES_IN,
      }),
      key: KeyRequest.TOKEN,
    };
  }

  private _generateUsername(names: string, lastnames: string): string {
    const end: number = lastnames.search(' ');

    if (end === -1) {
      return names[0].toLowerCase() + lastnames.toLowerCase();
    }

    return names[0].toLowerCase() + lastnames.substring(0, end).toLowerCase();
  }
}
