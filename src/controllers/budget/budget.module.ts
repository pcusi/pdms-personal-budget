import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MongoService } from '@gateway/mongo.service';
import { Budget, BudgetSchema } from '@schemas/budget.schema';
import { BudgetController } from '@controllers/budget/budget.controller';
import { BudgetService } from '@controllers/budget/budget.service';
import { BudgetCategory, BudgetCategorySchema } from '@schemas/category.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: Budget.name, schema: BudgetSchema },
      { name: BudgetCategory.name, schema: BudgetCategorySchema },
    ]),
  ],
  controllers: [BudgetController],
  providers: [BudgetService, MongoService],
})
export class BudgetModule {}
