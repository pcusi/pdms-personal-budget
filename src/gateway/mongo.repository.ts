import { Observable } from 'rxjs';
import { FilterQuery, Model } from 'mongoose';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';

export interface MongoRepository {
  /**
   * Return an observable array response
   *
   * @param {FilterQuery<T>} filterQuery
   * @param {Model<K>} model
   * @param {string} key
   * @template K
   * @template T
   * @returns {Observable<T>}
   * */
  findAll<K = object, T = object>(
    model: Model<K>,
    filterQuery?: FilterQuery<T>,
    key?: string,
  ): Observable<ResponseMapping>;

  /**
   * Return an observable from the type model object
   *
   * @param {FilterQuery<T>} filterQuery
   * @param {Model<K>} model
   * @template K
   * @template T
   * @returns {Observable<T>}
   * */
  findOne<K = object, T = object>(
    model: Model<K>,
    filterQuery: FilterQuery<T>,
  ): Observable<T>;

  /**
   * Return an observable of save from any model object
   *
   * @param {T} dto
   * @param {Model<K>} model
   * @param {string} message
   * @template K
   * @template T
   * @returns {Observable<ResponseMapping>}
   * */
  save<K = object, T = object>(
    model: Model<K>,
    dto: T,
    message?: string,
  ): Observable<ResponseMapping>;
}
