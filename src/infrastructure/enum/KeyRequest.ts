export enum KeyRequest {
  TOKEN = 'token',
  MESSAGE = 'message',
  BUDGETS = 'budgets',
}
