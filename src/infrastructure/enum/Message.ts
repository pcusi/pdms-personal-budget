export enum Message {
  BUDGET_CREATED = 'Budget was created successfully!',
  BUDGET_CATEGORY_CREATED = 'Budget category was created successfully!',
  FORBIDDEN = "You don't have permission to access this resource",
  PASSWORD_NOT_MATCH = 'Password provided not match.',
  UNAUTHORIZED = 'Access denied due to invalid credentials.',
  USER_NOT_FOUND = 'Username not found.',
}
