import { UserController } from '@controllers/user/user.controller';
import { Test, TestingModule } from '@nestjs/testing';
import { UserService } from '@controllers/user/user.service';
import { UserDTO } from '@dto/UserDTO';
import { getModelToken } from '@nestjs/mongoose';
import { User } from '@schemas/user.schema';
import { MongoService } from '@gateway/mongo.service';
import { of } from 'rxjs';
import { HttpStatus, INestApplication } from '@nestjs/common';
import { KeyRequest } from '@infrastructure/enum/KeyRequest';
import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Message } from '@infrastructure/enum/Message';
import * as request from 'supertest';

describe('UserController', () => {
  let userController: UserController;
  let userService: UserService;
  let mongoService: MongoService;
  let app: INestApplication;
  const user: UserDTO = {
    names: 'test',
    lastnames: 'test',
    username: 'pcusi',
    password: '123',
  };
  const message = 'Created!';

  const createUserSpy = jest.spyOn(UserService.prototype, 'createUser');
  const authUserSpy = jest.spyOn(UserService.prototype, 'auth');
  const findOneSpy = jest.spyOn(MongoService.prototype, 'findOne');

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      controllers: [UserController],
      providers: [
        UserService,
        MongoService,
        {
          provide: getModelToken(User.name),
          useValue: jest.fn(),
        },
      ],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();

    userController = moduleRef.get<UserController>(UserController);
    userService = await moduleRef.resolve(UserService);
    mongoService = await moduleRef.resolve(MongoService);

    createUserSpy.mockImplementation(async () => message);
    findOneSpy.mockImplementation(() => of({ ...user }));
  });

  afterEach(() => {
    createUserSpy.mockClear();
  });

  describe('root', () => {
    it('Should return "Created!" when controller has a success request', async () => {
      expect(await userController.createUser(user)).toBe(message);

      expect(createUserSpy).toBeCalledWith(user);
    });

    it('Should create an username when is provided two lastnames', async () => {
      expect(
        await userController.createUser({ ...user, lastnames: 'test test' }),
      ).toBe(message);

      expect(createUserSpy).toBeCalledWith({ ...user, lastnames: 'test test' });
      expect(createUserSpy).toBeCalledTimes(1);
    });

    it("should return a message when user isn't found with status code 400", async () => {
      authUserSpy.mockImplementation(
        async (): Promise<ResponseMapping> => ({
          statusCode: HttpStatus.NOT_FOUND,
          key: KeyRequest.MESSAGE,
          body: Message.USER_NOT_FOUND,
        }),
      );

      await request(app.getHttpServer())
        .post('/users/auth')
        .send({ username: user.username, password: user.password })
        .expect(({ body }) => {
          expect(body).toHaveProperty('message', Message.USER_NOT_FOUND);
        })
        .expect(HttpStatus.NOT_FOUND);
    });

    it('should return a response mapping object', async () => {
      authUserSpy.mockImplementation(
        async (): Promise<ResponseMapping> => ({
          statusCode: HttpStatus.BAD_REQUEST,
          key: KeyRequest.MESSAGE,
          body: Message.PASSWORD_NOT_MATCH,
        }),
      );

      await request(app.getHttpServer())
        .post('/users/auth')
        .send({ username: user.username, password: user.password })
        .expect(({ body }) => {
          expect(body).toHaveProperty('message', Message.PASSWORD_NOT_MATCH);
        })
        .expect(HttpStatus.BAD_REQUEST);
    });
  });
});
