export enum FieldsCatalog {
  BUDGET_CATEGORY_NAME = 'name',
  BUDGET_DESCRIPTION = 'description',
  BUDGET_AMOUNT = 'amount',
}

export const i18_fields: Record<FieldsCatalog, string> = {
  [FieldsCatalog.BUDGET_CATEGORY_NAME]: 'Nombre de categoría',
  [FieldsCatalog.BUDGET_AMOUNT]: 'Monto',
  [FieldsCatalog.BUDGET_DESCRIPTION]: 'Descripción',
};
