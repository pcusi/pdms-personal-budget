import { ResponseMapping } from '@infrastructure/mapping/ResponseMapping';
import { Response, Request } from 'express';
import { BudgetCategoryDTO, BudgetDTO } from '@dto/BudgetDTO';
import { FilterQuery } from "mongoose";

export interface BudgetRepository {
  createBudget(
    budget: BudgetDTO,
    res?: Response,
    req?: Request,
  ): Promise<ResponseMapping | Response>;

  createCategory(
    budgetCategoryDTO: BudgetCategoryDTO,
    res?: Response,
  ): Promise<ResponseMapping | Response>;

  getBudgets(
    filterQuery?: FilterQuery<BudgetDTO>,
    res?: Response,
    req?: Request,
  ): Promise<ResponseMapping | Response>;
}
